# Test task

## Стэк
- Next.js
- typescript
- AntDesign

## Версии зависимостей
- yarn 1.25
- Node.js 16.16

## Make команды

`start` - Запустить prod версию

`stop` - Остановить prod версию

`install` - Установить зависимости

`dev` - Запуск локального веб-сервера

`lint` - Запуск линтера

## Нативные команды

`yarn dev` - Запустится сервер на `http://localhost:3000`

`yarn lint --fix` - Запуск линтера

`yarn build` - Билд + проверка типов typescript

`yarn start` - Запуск билда
