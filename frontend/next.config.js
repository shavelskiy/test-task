const withLess = require("next-with-less");

module.exports = withLess({
  reactStrictMode: false,
  lessLoaderOptions: {},
  presets: ["next/babel"],
  images: {
    domains: ['localhost', 'localhost:8080', '*'],
  },
  env: {
    API_HOST: process.env.API_HOST,
  }
})
