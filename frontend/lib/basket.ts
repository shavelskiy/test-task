import {IOrderBasketItem} from '../types/order'

export const getBasketPrice = (basket: IOrderBasketItem[]) => {
  let result = 0.0
  basket.forEach(item => {
    result += Math.round(item.price * item.count * 100) / 100
  })

  return Math.round(result * 100) / 100
}