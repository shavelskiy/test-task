import cookie from 'js-cookie'
import {NextApiRequest} from 'next'

import {IBasketItem} from '../types/basketItem'

export const getCartFromRequest = (req: NextApiRequest): IBasketItem[] => {
  return JSON.parse(req.cookies.cart || `[]`)
}

export const getCart = (): IBasketItem[] => {
  return JSON.parse(cookie.get(`cart`) || `[]`)
}

export const addToCart = (productId: number) => {
  const cart = getCart()
  const hasItem = cart.filter(item => item.productId === productId).length > 0

  const newCart = hasItem
    ? cart.map(item => item.productId === productId ? {...item, count: item.count + 1} : item)
    : [...cart, {productId: productId, count: 1}]

  cookie.set(`cart`, JSON.stringify(newCart), { expires: 365 })
}

export const updateCart = (cart: IBasketItem[]) => {
  cookie.set(`cart`, JSON.stringify(cart), { expires: 365 })
}

export const clearCart = () => {
  cookie.remove(`cart`)
}
