const formatFractional = (price: number): string => {
  const fractional = Math.trunc((price - Math.trunc(price)) * 100)

  if (fractional <= 0) {
    return ``
  }

  return `,${fractional < 10 ? `0` : ``}${fractional}`
}

export const formatPrice = (price: number): string => {
  if (price <= 0) {
    return `0 ₽`
  }

  let result = []
  const fractional = formatFractional(price)

  while (price > 0) {
    price = price / 1000

    const part = Math.round((price - Math.trunc(price)) * 1000)
    price = Math.trunc(price)

    result.push(price <= 0 ? `${part}` : `${part < 100 ? `0` : ``}${part < 10 ? `0` : ``}${part}`)
  }

  return `${result.reverse().join(` `)}${fractional} ₽`
}

const months = [
  `января`, `февраля`, `марта`, `апреля`, `мая`, `июня`, `июля`, `августа`, `сентября`, `октября`, `ноября`, `декабря`,
]

export const formatDate = (data: string) => {
  const date = new Date(data)
  const currentDate = new Date()
  return `${date.getDate()} ${months[date.getMonth()]} ${currentDate.getFullYear() !== date.getFullYear() ? date.getFullYear() : ``}`
}

export const formatDatetime = (data: string) => {
  const date = new Date(data)
  return `${formatDate(data)} ${date.getHours()}:${date.getMinutes() < 10 ? `0` : ``}${date.getMinutes()}`
}
