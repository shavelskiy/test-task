import api from './api'

export const fetchOrder = () => api.get(`/order`)

export const createOrder = (data: any) => api.post(`/order`, data)
