import {message} from 'antd'
import axios from 'axios'


const api = axios.create({
  baseURL: process.env.API_HOST
})

api.interceptors.response.use(
  (response) => response,
  (error) => {
    message.error(`Произошла ошибка`)
    throw error
  }
)

export default api
