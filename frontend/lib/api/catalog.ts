import api from './api'

export const fetchCatalog = () => api.get(`/catalog`)
