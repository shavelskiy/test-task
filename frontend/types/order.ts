import {IProduct} from './catalog'

export interface IPerson {
  name: string,
  phone: string,
  email: string,
}

export interface IDelivery {
  city: string,
  address: string,
  comment: string,
}

export interface IOrderBasketItem {
  count: number,
  product: IProduct,
  price: number,
}

export interface IOrder {
  id: number,
  person: IPerson,
  delivery: IDelivery,
  basket: IOrderBasketItem[],
  createdAt: string,
}
