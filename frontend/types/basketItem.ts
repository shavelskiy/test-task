export interface IBasketItem {
  productId: number,
  count: number,
}
