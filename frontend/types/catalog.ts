export interface ICategory {
  id: number,
  name: string,
}

export interface IMeasure {
  id: number,
  name: string,
}

export interface IProduct {
  id: number,
  measure: IMeasure,
  name: string,
  quantity: number,
  price: number,
}

export interface ICatalogItem {
  category: ICategory,
  products: IProduct[],
}

export interface ICatalog {
  items: ICatalogItem[],
}
