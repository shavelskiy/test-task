import React from 'react'

import {NextPage} from 'next'

import Catalog from 'components/Catalog/Catalog'

import MainLayout from '../components/Layout/Layout'
import {fetchCatalog} from '../lib/api/catalog'
import {ICatalog} from '../types/catalog'


interface Props {
  catalog: ICatalog,
}

const IndexPage: NextPage<Props> = ({catalog}) => {
  return (
    <MainLayout title="Каталог товаров">
      <Catalog catalog={catalog}/>
    </MainLayout>
  )
}

export async function getServerSideProps() {
  const [res] = await Promise.all([
    fetchCatalog(),
  ])

  return {
    props: {
      catalog: res.data,
    },
  }
}

export default IndexPage
