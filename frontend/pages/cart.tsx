import React from 'react'

import {NextApiRequest, NextPage} from 'next'

import Checkout from '../components/Checkout/Checkout'
import MainLayout from '../components/Layout/Layout'
import {fetchCatalog} from '../lib/api/catalog'
import {getCartFromRequest} from '../lib/cart'
import {IBasketItem} from '../types/basketItem'
import {ICatalog} from '../types/catalog'


interface Props {
  catalog: ICatalog,
  cart: IBasketItem[],
}

const CartPage: NextPage<Props> = ({catalog, cart}) => {
  return (
    <MainLayout title="Корзина">
      <>
        <h1 style={{textAlign: `center`, margin: `20px 0`}}>Оформление заказа</h1>
        <Checkout catalog={catalog} initialCart={cart}/>
      </>
    </MainLayout>
  )
}

export async function getServerSideProps({req}: { req: NextApiRequest }) {
  const [res] = await Promise.all([
    fetchCatalog(),
  ])

  return {
    props: {
      catalog: res.data,
      cart: getCartFromRequest(req),
    },
  }
}

export default CartPage
