import React from 'react'

import {NextPage} from 'next'

import MainLayout from 'components/Layout/Layout'
import OrderList from 'components/OrderList/OrderList'
import {fetchOrder} from 'lib/api/order'
import {IOrder} from 'types/order'


interface Props {
  orders: IOrder[]
}

const OrdersPage: NextPage<Props> = ({orders}) => {
  return (
    <MainLayout title="Список заказов">
      <>
        <h1 style={{textAlign: `center`, margin: `20px 0`}}>Список заказов</h1>
        <OrderList orders={orders}/>
      </>
    </MainLayout>
  )
}

export async function getServerSideProps() {
  const [res] = await Promise.all([
    fetchOrder(),
  ])

  return {
    props: {
      orders: res.data,
    },
  }
}

export default OrdersPage
