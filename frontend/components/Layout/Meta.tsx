import React from 'react'

import Head from 'next/head'


type IProps = {
  title: string,
}

const Meta: React.FC<IProps> = ({title}) => {
  return (
    <Head>
      <title>{title}</title>
      <link rel="shortcut icon" href="favicon.ico"/>
    </Head>
  )
}

export default Meta
