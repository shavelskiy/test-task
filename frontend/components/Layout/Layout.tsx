import React, {ReactElement} from 'react'

import Meta from './Meta'
import Navigation from './Navigation'


type IProps = {
  title: string,
  children: ReactElement,
}

const MainLayout: React.FC<IProps> = ({title, children}) => {
  return (
    <>
      <Meta title={title}/>
      <div style={{backgroundColor: `#f5f5f5`}}>
        <Navigation/>
        <div style={{
          maxWidth: 1200,
          position: `relative`,
          margin: `auto`,
          minHeight: `calc(100vh - 46px)`,
          padding: `0 24px`
        }}>
          {children}
        </div>
      </div>
    </>
  )
}

export default MainLayout
