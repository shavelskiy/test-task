import React from 'react'

import {useRouter} from 'next/router'


const items = [
  {title: `Каталог`, link: `/`},
  {title: `Корзина`, link: `/cart`},
  {title: `Заказы`, link: `/orders`},
]

type IProps = {
  title: string,
  link: string,
}

const NavigationItem: React.FC<IProps> = ({title, link}) => {
  const router = useRouter()

  return (
    <div
      className={`navigation-item ${router.pathname === link ? `active` : ``}`}
      onClick={() => router.push(link)}
    >
      {title}
    </div>
  )
}

const Navigation: React.FC = () => {
  return (
    <div style={{backgroundColor: `#001529`, height: 46, display: `flex`, justifyContent: `center`}}>
      <div style={{maxWidth: 1200, width: `100%`, display: `flex`, justifyContent: `flex-end`, alignItems: `center`}}>
        {items.map(item => <NavigationItem key={item.link} title={item.title} link={item.link}/>)}
      </div>
    </div>
  )
}

export default Navigation
