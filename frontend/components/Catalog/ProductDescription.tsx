import React from 'react'

import {ShoppingCartOutlined} from '@ant-design/icons'
import {message} from 'antd'

import {addToCart} from 'lib/cart'
import {IProduct} from 'types/catalog'

import {formatPrice} from '../../lib/formatter'

type IProps = {
  product: IProduct
}

const ProductDescription: React.FC<IProps> = ({product}) => {
  const onClick = (productId: number) => {
    addToCart(productId)
    message.success(`Товар успешно добавлен в корзину`)
  }

  return (
    <div style={{display: `flex`, justifyContent: `space-between`, alignItems: `center`}}>
      <div>количество: {product.quantity} {product.measure.name}</div>
      <div style={{display: `flex`, alignItems: `center`}}>
        <div>цена: {formatPrice(product.price)}</div>
        <ShoppingCartOutlined style={{marginLeft: 10}} onClick={() => onClick(product.id)}/>
      </div>
    </div>
  )
}

export default ProductDescription