import React from 'react'

import {Tabs} from 'antd'

import {ICatalog} from 'types/catalog'

import CatalogBlock from './CatalogBlock'

type IProps = {
  catalog: ICatalog,
}

const Catalog: React.FC<IProps> = ({catalog}) => {
  return (
    <Tabs>
      {catalog.items.map((item, key) => (
        <Tabs.TabPane tab={item.category.name} key={key}>
          <CatalogBlock item={item}/>
        </Tabs.TabPane>
      ))}
    </Tabs>
  )
}

export default Catalog
