import React from 'react'

import {List} from 'antd'

import {ICatalogItem, IProduct} from 'types/catalog'

import ProductDescription from './ProductDescription'

type IProps = {
  item: ICatalogItem,
}


const CatalogBlock: React.FC<IProps> = ({item}) => {
  return (
    <List
      itemLayout="horizontal"
      dataSource={item.products}
      renderItem={(product: IProduct) => (
        <List.Item>
          <List.Item.Meta
            title={<span>{product.name}</span>}
            description={<ProductDescription product={product}/>}
          />
        </List.Item>
      )}
    />
  )
}

export default CatalogBlock
