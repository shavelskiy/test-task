import React from 'react'

import {Form, Input} from 'antd'


const DeliveryForm: React.FC = () => {
  return (
    <>
      <h3 style={{textAlign: `center`, margin: `20px 0`}}>Данные о доставке</h3>
      <Form.Item
        label="Город"
        name="delivery_city"
        rules={[{required: true, message: `Поле обязательно для заполнения`}]}
      >
        <Input/>
      </Form.Item>

      <Form.Item
        label="Адрес"
        name="delivery_address"
        rules={[{required: true, message: `Поле обязательно для заполнения`}]}
      >
        <Input/>
      </Form.Item>

      <Form.Item
        label="Комментарий"
        name="delivery_comment"
      >
        <Input/>
      </Form.Item>
    </>
  )
}

export default DeliveryForm
