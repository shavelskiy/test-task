import React from 'react'

import {DeleteOutlined} from '@ant-design/icons'
import {Table, InputNumber} from 'antd'

import {IBasketItem} from 'types/basketItem'
import {ICatalog, IProduct} from 'types/catalog'

import {formatPrice} from '../../lib/formatter'

type IProps = {
  catalog: ICatalog,
  cart: IBasketItem[],
  setCart: (cart: IBasketItem[]) => void,
}

type IRow = {
  product: IProduct,
  count: number,
  price: number,
}

const Cart: React.FC<IProps> = ({catalog, cart, setCart}) => {
  const changeCart = (productId: number, count: number) => {
    setCart(
      count > 0
        ? cart.map(item => item.productId === productId ? {...item, count: count} : item)
        : cart.filter(item => item.productId !== productId)
    )
  }

  const columns = [
    {
      title: `Товар`,
      render: (value: IRow) => value.product.name,
    },
    {
      title: `Цена`,
      render: (value: IRow) => formatPrice(value.product.price),
    },
    {
      title: `Количество`,
      render: (value: IRow) => (
        <div>
          <InputNumber
            value={value.count}
            min={1}
            max={9999}
            onChange={(v) => changeCart(value.product.id, v)}
          />
        </div>
      ),
    },
    {
      title: `Сумма`,
      render: (value: IRow) => formatPrice(value.price),
    },
    {
      title: ``,
      render: (value: IRow) => <DeleteOutlined onClick={() => changeCart(value.product.id, 0)}/>
    },
  ]

  const getProduct = (productId: number): IProduct => {
    return []
      // @ts-ignore
      .concat(...catalog.items.map(item => item.products))
      .filter((item: IProduct) => item.id === productId)[0]
  }

  const data = cart.map(item => {
    const product = getProduct(item.productId)
    return {
      key: item.productId,
      product: product,
      price: Math.round(product.price * item.count * 100) / 100,
      count: item.count,
    }
  })

  const getTotalPrice = () => {
    let result = 0.0
    data.forEach(value => {
      result += value.price
    })

    return Math.round(result * 100) / 100
  }

  return (
    <Table
      columns={columns}
      dataSource={data}
      pagination={false}
      footer={() => (
        <div style={{textAlign: `end`}}>
          Итого: {formatPrice(getTotalPrice())}
        </div>
      )}

    />
  )
}

export default Cart
