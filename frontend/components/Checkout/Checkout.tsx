import React, {useEffect, useState} from 'react'

import {Form, Button, message} from 'antd'
import {useRouter} from 'next/router'

import {createOrder} from 'lib/api/order'
import {clearCart, getCart, updateCart} from 'lib/cart'
import {IBasketItem} from 'types/basketItem'
import {ICatalog} from 'types/catalog'

import Cart from './Cart'
import DeliveryForm from './DeliveryForm'
import PersonForm from './PersonForm'



type IProps = {
  catalog: ICatalog,
  initialCart: IBasketItem[],
}

const Checkout: React.FC<IProps> = ({catalog, initialCart}) => {
  const [loading, setLoading] = useState<boolean>(false)
  const [cart, setCart] = useState<IBasketItem[]>(initialCart)

  useEffect(() => {
    updateCart(cart)
  }, [cart])

  const router = useRouter()

  const onFinish = (values: any) => {
    setLoading(true)

    createOrder({
      person: {
        name: values.person_name,
        phone: values.person_phone,
        email: values.person_email,
      },
      delivery: {
        city: values.delivery_city,
        address: values.delivery_address,
        comment: values.delivery_comment,
      },
      basket: getCart(),
    })
      .then(() => {
        message.success(`Заказ успешно оформлен`)
        clearCart()
        setTimeout(() => router.push(`/orders`), 1000)
      })
      .finally(() => setLoading(false))
  }

  return (
    <>
      <Cart catalog={catalog} cart={cart} setCart={setCart}/>
      {cart.length > 0 && (
        <>
          <Form
            name="checkout"
            labelCol={{span: 3}}
            wrapperCol={{span: 21}}
            onFinish={onFinish}
            autoComplete="off"
          >
            <PersonForm/>
            <DeliveryForm/>

            <Form.Item
              name="submit"
              wrapperCol={{offset: 3}}
            >
              <Button type="primary" htmlType="submit" loading={loading}>
                Оформить
              </Button>
            </Form.Item>
          </Form>
        </>
      )}
    </>
  )
}

export default Checkout