import React from 'react'

import {Form, Input} from 'antd'
import MaskedInput from 'antd-mask-input'


const PersonForm: React.FC = () => {
  return (
    <>
      <h3 style={{textAlign: `center`, margin: `20px 0`}}>Контактное лицо</h3>
      <Form.Item
        label="Имя"
        name="person_name"
        rules={[{required: true, message: `Поле обязательно для заполнения`}]}
      >
        <Input/>
      </Form.Item>

      <Form.Item
        label="Телефон"
        name="person_phone"
        rules={[{required: true, message: `Поле обязательно для заполнения`}]}
      >
        <MaskedInput mask="+0 000 000-00-00"/>
      </Form.Item>

      <Form.Item
        label="Почта"
        name="person_email"
        rules={[
          {type: `email`, message: `Введите корректное значение`},
          {required: true, message: `Поле обязательно для заполнения`},
        ]}
      >
        <Input/>
      </Form.Item>
    </>
  )
}

export default PersonForm
