import React from 'react'

import {Table} from 'antd'

import {formatPrice} from '../../lib/formatter'
import {IOrderBasketItem} from '../../types/order'

type IProps = {
  basket: IOrderBasketItem[],
}

type IRow = {
  item: IOrderBasketItem,
  price: number,
}

const OrderDetailBasket: React.FC<IProps> = ({basket}) => {
  const columns = [
    {
      title: `Товар`,
      render: (value: IRow) => value.item.product.name,
    },
    {
      title: `Цена`,
      render: (value: IRow) => <>{formatPrice(value.item.price)}</>,
    },
    {
      title: `Количество`,
      render: (value: IRow) => <>{value.item.count}</>,
    },
    {
      title: `Сумма`,
      render: (value: IRow) => <>{formatPrice(value.price)}</>,
    },
  ]

  const data = basket.map(item => {
    return {
      key: item.product.id,
      item: item,
      price:  Math.round(item.price * item.count * 100) / 100
    }
  })

  return (
    <Table
      columns={columns}
      dataSource={data}
      pagination={false}
    />
  )
}

export default OrderDetailBasket
