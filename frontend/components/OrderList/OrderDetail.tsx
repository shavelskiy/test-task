import React from 'react'

import {IOrder} from '../../types/order'
import OrderDetailBasket from './OrderDetailCart'
import OrderDetailInfo from './OrderDetailInfo'

type IProps = {
  order: IOrder,
}

const OrderDetail: React.FC<IProps> = ({order}) => {
  return (
    <>
      <OrderDetailBasket basket={order.basket}/>
      <OrderDetailInfo order={order}/>
    </>
  )
}

export default OrderDetail
