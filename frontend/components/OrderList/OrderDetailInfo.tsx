import React from 'react'

import {Descriptions} from 'antd'

import {IOrder} from '../../types/order'

type IProps = {
  order: IOrder,
}

const OrderDetailInfo: React.FC<IProps> = ({order}) => {
  return (
    <>
      <h3 style={{margin: `10px 0`}}>Клиент</h3>
      <Descriptions bordered style={{marginTop: 20}}>
        <Descriptions.Item label="Имя">{order.person.name}</Descriptions.Item>
        <Descriptions.Item label="Телефон">{order.person.phone}</Descriptions.Item>
        <Descriptions.Item label="Почта">{order.person.email}</Descriptions.Item>
      </Descriptions>

      <h3 style={{margin: `10px 0`}}>Доставка</h3>
      <Descriptions bordered style={{marginTop: 20}}>
        <Descriptions.Item label="Город">{order.delivery.city}</Descriptions.Item>
        <Descriptions.Item label="Адрес">{order.delivery.address}</Descriptions.Item>
        <Descriptions.Item label="Комментарий">{order.delivery.comment || `-`}</Descriptions.Item>
      </Descriptions>
    </>
  )
}

export default OrderDetailInfo
