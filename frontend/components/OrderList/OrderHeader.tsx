import React from 'react'

import {formatDatetime, formatPrice} from 'lib/formatter'
import {IOrder} from 'types/order'

import {getBasketPrice} from '../../lib/basket'


type IProps = {
  order: IOrder
}

const OrderList: React.FC<IProps> = ({order}) => {
  return (
    <div style={{display: `flex`, alignItems: `center`}}>
      <div style={{fontWeight: 600, fontSize: 14}}>#{order.id}</div>
      <div style={{fontSize: 12, marginLeft: 15}}>{formatDatetime(order.createdAt)}</div>
      <div style={{fontSize: 12, marginLeft: 15}}>{formatPrice(getBasketPrice(order.basket))}</div>
    </div>
  )
}

export default OrderList