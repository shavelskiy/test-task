import React from 'react'

import {Collapse} from 'antd'

import {IOrder} from 'types/order'

import OrderDetail from './OrderDetail'
import OrderHeader from './OrderHeader'


type IProps = {
  orders: IOrder[]
}

const OrderList: React.FC<IProps> = ({orders}) => {
  return (
    <>
      <Collapse>
        {orders.map((order) => (
          <Collapse.Panel
            key={order.id}
            header={<OrderHeader order={order}/>}
          >
            <OrderDetail order={order}/>
          </Collapse.Panel>
        ))}
      </Collapse>
    </>
  )
}

export default OrderList