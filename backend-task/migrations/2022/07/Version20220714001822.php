<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220714001822 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE order_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE order_basket_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE order_delivery_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE order_person_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE category (id INT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE measure (id INT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE "order" (id INT NOT NULL, person_id INT DEFAULT NULL, delivery_id INT DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_F5299398217BBB47 ON "order" (person_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_F529939812136921 ON "order" (delivery_id)');
        $this->addSql('CREATE TABLE order_basket (id INT NOT NULL, product_id BIGINT DEFAULT NULL, order_id INT DEFAULT NULL, count INT NOT NULL, price DOUBLE PRECISION NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_E1C940AE4584665A ON order_basket (product_id)');
        $this->addSql('CREATE INDEX IDX_E1C940AE8D9F6D38 ON order_basket (order_id)');
        $this->addSql('CREATE TABLE order_delivery (id INT NOT NULL, city VARCHAR(255) NOT NULL, address VARCHAR(255) NOT NULL, comment VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE order_person (id INT NOT NULL, name VARCHAR(255) NOT NULL, phone VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE product (id BIGINT NOT NULL, category_id INT DEFAULT NULL, measure_id INT DEFAULT NULL, hidden BOOLEAN NOT NULL, name VARCHAR(255) NOT NULL, quantity DOUBLE PRECISION NOT NULL, price DOUBLE PRECISION NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_D34A04AD12469DE2 ON product (category_id)');
        $this->addSql('CREATE INDEX IDX_D34A04AD5DA37D00 ON product (measure_id)');
        $this->addSql('ALTER TABLE "order" ADD CONSTRAINT FK_F5299398217BBB47 FOREIGN KEY (person_id) REFERENCES order_person (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "order" ADD CONSTRAINT FK_F529939812136921 FOREIGN KEY (delivery_id) REFERENCES order_delivery (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE order_basket ADD CONSTRAINT FK_E1C940AE4584665A FOREIGN KEY (product_id) REFERENCES product (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE order_basket ADD CONSTRAINT FK_E1C940AE8D9F6D38 FOREIGN KEY (order_id) REFERENCES "order" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD12469DE2 FOREIGN KEY (category_id) REFERENCES category (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD5DA37D00 FOREIGN KEY (measure_id) REFERENCES measure (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE product DROP CONSTRAINT FK_D34A04AD12469DE2');
        $this->addSql('ALTER TABLE product DROP CONSTRAINT FK_D34A04AD5DA37D00');
        $this->addSql('ALTER TABLE order_basket DROP CONSTRAINT FK_E1C940AE8D9F6D38');
        $this->addSql('ALTER TABLE "order" DROP CONSTRAINT FK_F529939812136921');
        $this->addSql('ALTER TABLE "order" DROP CONSTRAINT FK_F5299398217BBB47');
        $this->addSql('ALTER TABLE order_basket DROP CONSTRAINT FK_E1C940AE4584665A');
        $this->addSql('DROP SEQUENCE order_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE order_basket_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE order_delivery_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE order_person_id_seq CASCADE');
        $this->addSql('DROP TABLE category');
        $this->addSql('DROP TABLE measure');
        $this->addSql('DROP TABLE "order"');
        $this->addSql('DROP TABLE order_basket');
        $this->addSql('DROP TABLE order_delivery');
        $this->addSql('DROP TABLE order_person');
        $this->addSql('DROP TABLE product');
    }
}
