<?php

declare(strict_types=1);

namespace App\Application;

interface CommandHandler
{
    public function __invoke(Command $command): void;
    public function support(Command $command): bool;
}
