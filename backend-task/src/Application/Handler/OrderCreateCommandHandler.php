<?php

declare(strict_types=1);

namespace App\Application\Handler;

use App\Api\Request\OrderCreateRequest;
use App\Application\Command;
use App\Application\Command\OrderCreateCommand;
use App\Application\CommandHandler;
use App\Exception\CommandException;
use App\Infrastructure\Entity\Order;
use App\Infrastructure\Entity\OrderBasket;
use App\Infrastructure\Entity\OrderDelivery;
use App\Infrastructure\Entity\OrderPerson;
use App\Infrastructure\Repository\OrderRepository;
use App\Infrastructure\Repository\ProductRepository;

class OrderCreateCommandHandler implements CommandHandler
{
    public function __construct(
        private OrderRepository $orderRepository,
        private ProductRepository $productRepository,
    ) {
    }

    public function __invoke(Command $command): void
    {
        if (!$command instanceof OrderCreateCommand) {
            throw CommandException::commandHandler();
        }

        $order = $this->buildOrder($command->getRequest());

        $this->orderRepository->save($order);
        $command->setResult($order);
    }

    public function buildOrder(OrderCreateRequest $request): Order
    {
        $person = (new OrderPerson())
            ->setName($request->getPerson()->getName())
            ->setPhone($request->getPerson()->getPhone())
            ->setEmail($request->getPerson()->getEmail())
        ;

        $delivery = (new OrderDelivery())
            ->setCity($request->getDelivery()->getCity())
            ->setAddress($request->getDelivery()->getAddress())
            ->setComment($request->getDelivery()->getComment())
        ;

        $order = new Order($person, $delivery);

        foreach ($request->getBasket() as $item) {
            $product = $this->productRepository->find($item->getProductId());
            $basket = (new OrderBasket($product, $order))
                ->setCount($item->getCount())
                ->setPrice($product->getPrice())
            ;

            $order->addBasket($basket);
        }

        return $order;
    }

    public function support(Command $command): bool
    {
        return $command instanceof OrderCreateCommand;
    }
}
