<?php

declare(strict_types=1);

namespace App\Application\Handler;

use App\Application\Command;
use App\Application\Command\CatalogQuery;
use App\Application\CommandHandler;
use App\Exception\CommandException;
use App\Infrastructure\Repository\ProductRepository;

class CatalogQueryHandler implements CommandHandler
{
    public function __construct(
        private ProductRepository $productRepository,
    ) {
    }

    public function __invoke(Command $command): void
    {
        if (!$command instanceof CatalogQuery) {
            throw CommandException::commandHandler();
        }

        $command->setResult(
            $this->productRepository->findActive(),
        );
    }

    public function support(Command $command): bool
    {
        return $command instanceof CatalogQuery;
    }
}
