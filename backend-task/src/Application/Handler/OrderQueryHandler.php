<?php

declare(strict_types=1);

namespace App\Application\Handler;

use App\Application\Command;
use App\Application\Command\OrderQuery;
use App\Application\CommandHandler;
use App\Exception\CommandException;
use App\Infrastructure\Repository\OrderRepository;

class OrderQueryHandler implements CommandHandler
{
    public function __construct(
        private OrderRepository $orderRepository,
    ) {
    }

    public function __invoke(Command $command): void
    {
        if (!$command instanceof OrderQuery) {
            throw CommandException::commandHandler();
        }

        $command->setResult(
            $this->orderRepository->findForList(),
        );
    }

    public function support(Command $command): bool
    {
        return $command instanceof OrderQuery;
    }
}
