<?php

declare(strict_types=1);

namespace App\Application\Command;

use App\Api\Request\OrderCreateRequest;
use App\Application\Command;
use App\Exception\CommandException;
use App\Infrastructure\Entity\Order;

class OrderCreateCommand extends Command
{
    private ?Order $result = null;

    public function __construct(
        private OrderCreateRequest $request,
    ) {
    }

    public function getRequest(): OrderCreateRequest
    {
        return $this->request;
    }

    public function getResult(): Order
    {
        return $this->result ?? throw CommandException::result();
    }

    public function setResult(Order $result): void
    {
        $this->result = $result;
    }
}
