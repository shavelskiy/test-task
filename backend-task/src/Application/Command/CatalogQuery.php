<?php

declare(strict_types=1);

namespace App\Application\Command;

use App\Application\Command;
use App\Infrastructure\Entity\Product;

class CatalogQuery extends Command
{
    /** @var Product[] */
    private array $result = [];

    /** @return Product[] */
    public function getResult(): array
    {
        return $this->result;
    }

    /** @param Product[] $result */
    public function setResult(array $result): void
    {
        $this->result = $result;
    }
}
