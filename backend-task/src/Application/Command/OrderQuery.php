<?php

declare(strict_types=1);

namespace App\Application\Command;

use App\Application\Command;
use App\Infrastructure\Entity\Order;

class OrderQuery extends Command
{
    /** @var Order[] */
    private array $result = [];

    /** @return Order[] */
    public function getResult(): array
    {
        return $this->result;
    }

    /** @param Order[] $result */
    public function setResult(array $result): void
    {
        $this->result = $result;
    }
}
