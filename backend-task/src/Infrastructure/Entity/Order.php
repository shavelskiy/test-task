<?php

declare(strict_types=1);

namespace App\Infrastructure\Entity;

use App\Exception\RepositoryException;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\Table(name: '`order`')]
class Order
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id = null;

    #[ORM\OneToOne(targetEntity: OrderPerson::class)]
    private OrderPerson $person;

    #[ORM\OneToOne(targetEntity: OrderDelivery::class)]
    private OrderDelivery $delivery;

    /** @var Collection<int, OrderBasket> */
    #[ORM\OneToMany(mappedBy: 'order', targetEntity: OrderBasket::class)]
    private Collection $basket;

    #[ORM\Column(type: 'datetime')]
    private \DateTime $createdAt;

    public function __construct(OrderPerson $person, OrderDelivery $delivery)
    {
        $this->person = $person;
        $this->delivery = $delivery;
        $this->basket = new ArrayCollection();
        $this->createdAt = new \DateTime();
    }

    public function getId(): int
    {
        return $this->id ?? throw RepositoryException::notFullEntity('order_id');
    }

    public function getPerson(): OrderPerson
    {
        return $this->person;
    }

    public function setPerson(OrderPerson $person): self
    {
        $this->person = $person;
        return $this;
    }

    public function getDelivery(): OrderDelivery
    {
        return $this->delivery;
    }

    public function setDelivery(OrderDelivery $delivery): self
    {
        $this->delivery = $delivery;
        return $this;
    }

    /** @return Collection<int, OrderBasket> */
    public function getBasket(): Collection
    {
        return $this->basket;
    }

    public function addBasket(OrderBasket $item): self
    {
        $this->basket->add($item);
        return $this;
    }

    public function getCreatedAt(): \DateTimeImmutable
    {
        return \DateTimeImmutable::createFromMutable($this->createdAt);
    }
}
