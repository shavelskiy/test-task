<?php

declare(strict_types=1);

namespace App\Infrastructure\Entity;

use App\Exception\RepositoryException;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class OrderPerson
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id = null;

    #[ORM\Column(type: 'string')]
    private string $name = '';

    #[ORM\Column(type: 'string')]
    private string $phone = '';

    #[ORM\Column(type: 'string')]
    private string $email = '';

    public function getId(): int
    {
        return $this->id ?? throw RepositoryException::notFullEntity('order_peron_id');
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;
        return $this;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;
        return $this;
    }
}
