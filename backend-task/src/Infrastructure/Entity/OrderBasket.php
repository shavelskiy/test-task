<?php

declare(strict_types=1);

namespace App\Infrastructure\Entity;

use App\Exception\RepositoryException;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class OrderBasket
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id = null;

    #[ORM\ManyToOne(targetEntity: Product::class)]
    private Product $product;

    #[ORM\ManyToOne(targetEntity: Order::class, inversedBy: 'basket')]
    private Order $order;

    #[ORM\Column(type: 'integer')]
    private int $count = 1;

    #[ORM\Column(type: 'float')]
    private float $price = 0.0;

    public function __construct(
        Product $product,
        Order $order,
    ) {
        $this->product = $product;
        $this->order = $order;
    }

    public function getId(): int
    {
        return $this->id ?? throw RepositoryException::notFullEntity('basket_item_id');
    }

    public function getProduct(): Product
    {
        return $this->product;
    }

    public function setProduct(Product $product): self
    {
        $this->product = $product;
        return $this;
    }

    public function getOrder(): Order
    {
        return $this->order;
    }

    public function setOrder(Order $order): self
    {
        $this->order = $order;
        return $this;
    }

    public function getCount(): int
    {
        return $this->count;
    }

    public function setCount(int $count): self
    {
        $this->count = $count;
        return $this;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;
        return $this;
    }
}
