<?php

declare(strict_types=1);

namespace App\Infrastructure\Entity;

use App\Exception\RepositoryException;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class Measure
{
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'NONE')]
    #[ORM\Column(type: 'integer')]
    private ?int $id = null;

    #[ORM\Column(type: 'string')]
    private string $name = '';

    public function getId(): int
    {
        return $this->id ?? throw RepositoryException::notFullEntity('measure_id');
    }

    public function setId(int $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }
}
