<?php

declare(strict_types=1);

namespace App\Infrastructure\Entity;

use App\Exception\RepositoryException;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class OrderDelivery
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id = null;

    #[ORM\Column(type: 'string')]
    private string $city = '';

    #[ORM\Column(type: 'string')]
    private string $address = '';

    #[ORM\Column(type: 'string', nullable: true)]
    private ?string $comment = '';

    public function getId(): int
    {
        return $this->id ?? throw RepositoryException::notFullEntity('order_delivery_id');
    }

    public function getCity(): string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;
        return $this;
    }

    public function getAddress(): string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;
        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;
        return $this;
    }
}
