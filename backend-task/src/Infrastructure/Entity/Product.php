<?php

declare(strict_types=1);

namespace App\Infrastructure\Entity;

use App\Exception\RepositoryException;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class Product
{
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'NONE')]
    #[ORM\Column(type: 'bigint')]
    private ?int $id = null;

    #[ORM\ManyToOne(targetEntity: Category::class)]
    private Category $category;

    #[ORM\ManyToOne(targetEntity: Measure::class)]
    private Measure $measure;

    #[ORM\Column(type: 'boolean')]
    private bool $hidden = false;

    #[ORM\Column(type: 'string')]
    private string $name = '';

    #[ORM\Column(type: 'float')]
    private float $quantity = 0.0;

    #[ORM\Column(type: 'float')]
    private float $price = 0.0;

    public function __construct(Category $category, Measure $measure)
    {
        $this->category = $category;
        $this->measure = $measure;
    }

    public function getId(): int
    {
        return $this->id ?? throw RepositoryException::notFullEntity('product_id');
    }

    public function setId(int $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getCategory(): Category
    {
        return $this->category;
    }

    public function setCategory(Category $category): self
    {
        $this->category = $category;
        return $this;
    }

    public function getMeasure(): Measure
    {
        return $this->measure;
    }

    public function setMeasure(Measure $measure): self
    {
        $this->measure = $measure;
        return $this;
    }

    public function isHidden(): bool
    {
        return $this->hidden;
    }

    public function setHidden(bool $hidden): self
    {
        $this->hidden = $hidden;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function getQuantity(): float
    {
        return $this->quantity;
    }

    public function setQuantity(float $quantity): self
    {
        $this->quantity = $quantity;
        return $this;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;
        return $this;
    }
}
