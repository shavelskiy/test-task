<?php

declare(strict_types=1);

namespace App\Infrastructure;

use App\Application\Command;
use App\Application\CommandBusInterface;
use App\Application\CommandHandler;
use App\Exception\CommandException;
use Symfony\Component\DependencyInjection\Attribute\TaggedIterator;

class CommandBus implements CommandBusInterface
{
    /** @var iterable<CommandHandler> */
    private iterable $handlers;

    /**
     * @param iterable<CommandHandler> $handlers
     */
    public function __construct(
        #[TaggedIterator('app.infrastructure.command_handler')] iterable $handlers,
    ) {
        $this->handlers = $handlers;
    }

    public function handle(Command $command): void
    {
        foreach ($this->handlers as $handler) {
            if ($handler->support($command)) {
                $handler($command);
                return;
            }
        }

        throw CommandException::handlerNotFound(get_debug_type($command));
    }
}
