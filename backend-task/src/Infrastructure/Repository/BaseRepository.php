<?php

declare(strict_types=1);

namespace App\Infrastructure\Repository;

use App\Exception\RepositoryException;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @template T
 * @template-extends ServiceEntityRepository<T>
 */
abstract class BaseRepository extends ServiceEntityRepository
{
    /**
     * @psalm-return T
     *
     * @param mixed      $id
     * @param null|mixed $lockMode
     * @param null|mixed $lockVersion
     */
    public function find($id, $lockMode = null, $lockVersion = null): object
    {
        return parent::find($id) ?? throw RepositoryException::notFound();
    }

    /**
     * @psalm-return T
     *
     * @psalm-param array<string, mixed> $criteria
     * @psalm-param array<string, string>|null $orderBy
     */
    public function findOneBy(array $criteria, ?array $orderBy = null): object
    {
        return parent::findOneBy($criteria, $orderBy) ?? throw RepositoryException::notFound();
    }
}
