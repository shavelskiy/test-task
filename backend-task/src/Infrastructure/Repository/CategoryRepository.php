<?php

declare(strict_types=1);

namespace App\Infrastructure\Repository;

use App\Infrastructure\Entity\Category;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @template-extends BaseRepository<Category>
 */
class CategoryRepository extends BaseRepository
{
    public function __construct(
        ManagerRegistry $registry,
    ) {
        parent::__construct($registry, Category::class);
    }
}
