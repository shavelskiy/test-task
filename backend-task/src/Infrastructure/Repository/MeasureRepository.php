<?php

declare(strict_types=1);

namespace App\Infrastructure\Repository;

use App\Infrastructure\Entity\Measure;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @template-extends BaseRepository<Measure>
 */
class MeasureRepository extends BaseRepository
{
    public function __construct(
        ManagerRegistry $registry,
    ) {
        parent::__construct($registry, Measure::class);
    }
}
