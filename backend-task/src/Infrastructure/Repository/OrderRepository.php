<?php

declare(strict_types=1);

namespace App\Infrastructure\Repository;

use App\Exception\RepositoryException;
use App\Infrastructure\Entity\Order;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @template-extends BaseRepository<Order>
 */
class OrderRepository extends BaseRepository
{
    public function __construct(
        ManagerRegistry $registry,
    ) {
        parent::__construct($registry, Order::class);
    }

    /** @return Order[] */
    public function findForList(): array
    {
        /** @var Order[] $queryResult */
        $queryResult = $this
            ->createQueryBuilder('o')
            ->orderBy('o.createdAt', 'desc')
            ->getQuery()
            ->getResult()
        ;

        $result = [];

        foreach ($queryResult as $order) {
            $result[] = $order;
        }

        return $result;
    }

    public function save(Order $order): void
    {
        try {
            $order->getId();
        } catch (RepositoryException) {
            $this->_em->persist($order);
            $this->_em->persist($order->getPerson());
            $this->_em->persist($order->getDelivery());
            foreach ($order->getBasket() as $basket) {
                $this->_em->persist($basket);
            }
        }

        $this->_em->flush();
    }
}
