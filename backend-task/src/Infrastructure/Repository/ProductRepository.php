<?php

declare(strict_types=1);

namespace App\Infrastructure\Repository;

use App\Infrastructure\Entity\Product;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @template-extends BaseRepository<Product>
 */
class ProductRepository extends BaseRepository
{
    public function __construct(
        ManagerRegistry $registry,
    ) {
        parent::__construct($registry, Product::class);
    }

    /** @return Product[] */
    public function findActive(): array
    {
        return $this->findBy(['hidden' => false]);
    }
}
