<?php

declare(strict_types=1);

namespace App\Infrastructure\DataFixtures;

use App\Infrastructure\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class CategoryFixtures extends Fixture
{
    private const DATA = [
        [
            'id' => 1,
            'name' => 'Готовые блюда',
        ],
        [
            'id' => 2,
            'name' => 'Мясо',
        ],
        [
            'id' => 3,
            'name' => 'Овощи и фрукты',
        ],
        [
            'id' => 4,
            'name' => 'Колбасная продукция и деликатесы',
        ],
        [
            'id' => 5,
            'name' => 'Яйца',
        ],
        [
            'id' => 6,
            'name' => 'Молоко и молочная продукция',
        ],

        [
            'id' => 7,
            'name' => 'Сыры',
        ],

        [
            'id' => 8,
            'name' => 'Рыба и морепродукты',
        ],

        [
            'id' => 9,
            'name' => 'Полуфабрикаты',
        ],

        [
            'id' => 10,
            'name' => 'Бакалея',
        ],
    ];

    public function load(ObjectManager $manager): void
    {
        foreach (self::DATA as $item) {
            $category = new Category();
            $category->setId($item['id']);
            $category->setName($item['name']);

            $manager->persist($category);
        }

        $manager->flush();
    }
}
