<?php

declare(strict_types=1);

namespace App\Infrastructure\DataFixtures;

use App\Infrastructure\Entity\Measure;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class MeasureFixtures extends Fixture
{
    private const DATA = [
        [
            'id' => 0,
            'name' => 'шт',
        ],
        [
            'id' => 1,
            'name' => 'кг',
        ],
        [
            'id' => 2,
            'name' => 'уп',
        ],
        [
            'id' => 3,
            'name' => 'л',
        ],
        [
            'id' => 6,
            'name' => '100г',
        ],
    ];

    public function load(ObjectManager $manager): void
    {
        foreach (self::DATA as $item) {
            $measure = new Measure();
            $measure->setId($item['id']);
            $measure->setName($item['name']);

            $manager->persist($measure);
        }

        $manager->flush();
    }
}
