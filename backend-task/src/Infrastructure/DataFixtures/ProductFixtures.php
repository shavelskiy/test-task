<?php

declare(strict_types=1);

namespace App\Infrastructure\DataFixtures;

use App\Infrastructure\Entity\Product;
use App\Infrastructure\Repository\CategoryRepository;
use App\Infrastructure\Repository\MeasureRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class ProductFixtures extends Fixture implements DependentFixtureInterface
{
    private const DATA = [
        ['id' => 3071300004, 'category' => 3, 'measure' => 1, 'hidden' => 0, 'name' => 'Ананас весовой', 'quantity' => 16.777, 'price' => 89.99],
        ['id' => 3072100005, 'category' => 3, 'measure' => 0, 'hidden' => 0, 'name' => 'Базилик свежий зеленый 20г', 'quantity' => 7.000, 'price' => 49.99],
        ['id' => 3071600002, 'category' => 3, 'measure' => 1, 'hidden' => 1, 'name' => 'Гранат импортный', 'quantity' => 14.028, 'price' => 185.99],
        ['id' => 1050400012, 'category' => 3, 'measure' => 0, 'hidden' => 0, 'name' => 'Зелень свежая базилик, укроп, петрушка,кинза пряно-вкусовая 115г', 'quantity' => 20.000, 'price' => 99.99],
        ['id' => 1050400034, 'category' => 3, 'measure' => 0, 'hidden' => 0, 'name' => 'Зелень свежая лук, укроп, кинза Кавказская 70г', 'quantity' => 20.000, 'price' => 59.99],
        ['id' => 1050400017, 'category' => 3, 'measure' => 0, 'hidden' => 0, 'name' => 'Зелень свежая лук, укроп, петрушка 100г', 'quantity' => 18.000, 'price' => 59.99],
        ['id' => 1050400018, 'category' => 3, 'measure' => 0, 'hidden' => 0, 'name' => 'Зелень свежая лук, укроп, петрушка 250г', 'quantity' => 13.000, 'price' => 119.99],
        ['id' => 1050400032, 'category' => 3, 'measure' => 0, 'hidden' => 0, 'name' => 'Зелень свежая лук, укроп, редис Окрошка 200г', 'quantity' => 16.000, 'price' => 79.99],
        ['id' => 1050400033, 'category' => 3, 'measure' => 0, 'hidden' => 0, 'name' => 'Зелень свежая укроп, петрушка Ароматная 60г', 'quantity' => 21.000, 'price' => 59.99],
        ['id' => 7040500005, 'category' => 8, 'measure' => 0, 'hidden' => 0, 'name' => 'Кета (лосось дальневосточный) филе-кусок слобосолёная 150г ТМ Рыбмастер', 'quantity' => 27.000, 'price' => 199.99],
        ['id' => 7040500006, 'category' => 8, 'measure' => 0, 'hidden' => 0, 'name' => 'Кета (лосось дальневосточный) филе-кусок слобосолёная 200г ТМ Рыбмастер', 'quantity' => 8.000, 'price' => 269.99],
        ['id' => 1050400007, 'category' => 3, 'measure' => 0, 'hidden' => 0, 'name' => 'Лук зеленый 100г', 'quantity' => 13.000, 'price' => 35.99],
        ['id' => 1050400009, 'category' => 3, 'measure' => 0, 'hidden' => 0, 'name' => 'Лук зеленый свежий 50г', 'quantity' => 20.000, 'price' => 25.99],
        ['id' => 3070100025, 'category' => 3, 'measure' => 0, 'hidden' => 0, 'name' => 'Манго шт', 'quantity' => 21.000, 'price' => 149.99],
        ['id' => 1050300031, 'category' => 3, 'measure' => 0, 'hidden' => 0, 'name' => 'Овощи салатные свежие Руккола/Романо 75г Белая Дача', 'quantity' => 5.000, 'price' => 79.99],
        ['id' => 3070200005, 'category' => 3, 'measure' => 1, 'hidden' => 0, 'name' => 'Овощи свежие Баклажаны', 'quantity' => 65.297, 'price' => 129.99],
        ['id' => 3070200024, 'category' => 3, 'measure' => 1, 'hidden' => 0, 'name' => 'Овощи свежие капуста краснокoчанная', 'quantity' => 18.826, 'price' => 74.99],
        ['id' => 1050400023, 'category' => 3, 'measure' => 0, 'hidden' => 0, 'name' => 'Петрушка свежая охлажденная 100г', 'quantity' => 11.000, 'price' => 29.99],
        ['id' => 1050400024, 'category' => 3, 'measure' => 0, 'hidden' => 0, 'name' => 'Петрушка свежая охлажденная 250г', 'quantity' => 11.000, 'price' => 59.99],
        ['id' => 1050400025, 'category' => 3, 'measure' => 0, 'hidden' => 0, 'name' => 'Петрушка свежая охлажденная 50г', 'quantity' => 7.000, 'price' => 34.99],
        ['id' => 7040600002, 'category' => 8, 'measure' => 0, 'hidden' => 0, 'name' => 'Пищевая рыбная продукция Кета холодного копчения филе-кусок 150г Русское Море', 'quantity' => 20.000, 'price' => 199.99],
        ['id' => 3070200195, 'category' => 3, 'measure' => 0, 'hidden' => 0, 'name' => 'Помидор Черри 250г стакан', 'quantity' => 10.000, 'price' => 109.99],
        ['id' => 7021000010, 'category' => 8, 'measure' => 0, 'hidden' => 0, 'name' => 'Рулет горячего копчения из крабового мяса 400г в/у', 'quantity' => 7.000, 'price' => 194.99],
        ['id' => 7031400011, 'category' => 8, 'measure' => 1, 'hidden' => 0, 'name' => 'Рыба Горбуша балык холодного копчения', 'quantity' => 25.022, 'price' => 589.99],
        ['id' => 7031400005, 'category' => 8, 'measure' => 0, 'hidden' => 0, 'name' => 'Рыба Горбуша Косичка холодного копчения 300г', 'quantity' => 7.000, 'price' => 199.99],
        ['id' => 7020200003, 'category' => 8, 'measure' => 1, 'hidden' => 0, 'name' => 'Рыба Горбуша холодного копчения без головы', 'quantity' => 32.268, 'price' => 479.99],
        ['id' => 7031400006, 'category' => 8, 'measure' => 0, 'hidden' => 0, 'name' => 'Рыба Горбуша/Скумбрия Ассорти Косичка холодного копчения 300г', 'quantity' => 6.000, 'price' => 159.99],
        ['id' => 7042200001, 'category' => 8, 'measure' => 1, 'hidden' => 0, 'name' => 'Рыба горячего копчения Лещ', 'quantity' => 8.438, 'price' => 329.99],
        ['id' => 7020100027, 'category' => 8, 'measure' => 1, 'hidden' => 0, 'name' => 'Рыба Камбала холодного копчения без головы 1/2', 'quantity' => 7.450, 'price' => 389.99],
        ['id' => 7020500005, 'category' => 8, 'measure' => 1, 'hidden' => 0, 'name' => 'Рыба Килька холодного копчения Соврыбторг', 'quantity' => 11.496, 'price' => 309.99],
        ['id' => 1110200002, 'category' => 8, 'measure' => 1, 'hidden' => 0, 'name' => 'Рыба Лещ холодного копчения', 'quantity' => 11.658, 'price' => 339.99],
        ['id' => 7022200011, 'category' => 8, 'measure' => 0, 'hidden' => 0, 'name' => 'Рыба Масляная холодного копчения филе-кусок 300г', 'quantity' => 15.000, 'price' => 449.99],
        ['id' => 7031000007, 'category' => 8, 'measure' => 0, 'hidden' => 0, 'name' => 'Рыба Мойва холодного копчения 200г лоток ТМ Фрегат', 'quantity' => 7.000, 'price' => 119.99],
        ['id' => 1110200040, 'category' => 8, 'measure' => 1, 'hidden' => 0, 'name' => 'Рыба Путассу холодного копчения тушка', 'quantity' => 10.918, 'price' => 329.99],
        ['id' => 7020100043, 'category' => 8, 'measure' => 1, 'hidden' => 0, 'name' => 'Рыба Сардина (иваси) холодного копчения', 'quantity' => 8.132, 'price' => 269.99],
        ['id' => 7020100025, 'category' => 8, 'measure' => 1, 'hidden' => 0, 'name' => 'Рыба Сельдь холодного копчения +400', 'quantity' => 7.422, 'price' => 379.99],
        ['id' => 7023300001, 'category' => 8, 'measure' => 0, 'hidden' => 0, 'name' => 'Рыба Скумбрия VICI Атлантическая холодного копчения кусочки без головы крупная', 'quantity' => 9.000, 'price' => 399.99],
        ['id' => 7020100023, 'category' => 8, 'measure' => 1, 'hidden' => 0, 'name' => 'Рыба Скумбрия холодного копчения без головы 300+', 'quantity' => 19.102, 'price' => 349.99],
        ['id' => 7022800014, 'category' => 8, 'measure' => 1, 'hidden' => 0, 'name' => 'Рыба Скумбрия холодного копчения потрошеная обезглавленная 450+', 'quantity' => 43.604, 'price' => 529.99],
        ['id' => 7040500004, 'category' => 8, 'measure' => 0, 'hidden' => 0, 'name' => 'Рыба Форель радужная слабосоленая филе-кусок 240г ТМ Рыбмастер', 'quantity' => 10.000, 'price' => 449.99],
        ['id' => 7023200001, 'category' => 8, 'measure' => 1, 'hidden' => 0, 'name' => 'Рыба Язь холодного копчения пласт', 'quantity' => 6.882, 'price' => 419.99],
        ['id' => 7030600020, 'category' => 8, 'measure' => 0, 'hidden' => 0, 'name' => 'Сардина Иваси неразделанная холодного копчения 300г ТМ Фрегат', 'quantity' => 13.000, 'price' => 89.99],
        ['id' => 1050300023, 'category' => 3, 'measure' => 0, 'hidden' => 0, 'name' => 'Свежие листья салата Шпинат 75г', 'quantity' => 5.000, 'price' => 69.99],
        ['id' => 3070100081, 'category' => 3, 'measure' => 1, 'hidden' => 0, 'name' => 'Свежие фрукты Помело', 'quantity' => 12.134, 'price' => 399.99],
        ['id' => 1110800038, 'category' => 8, 'measure' => 1, 'hidden' => 0, 'name' => 'Сельдь слабосоленая +400 крупная', 'quantity' => 34.236, 'price' => 269.99],
        ['id' => 3070200109, 'category' => 3, 'measure' => 1, 'hidden' => 0, 'name' => 'Тыква', 'quantity' => 25.662, 'price' => 119.99],
        ['id' => 1050400027, 'category' => 3, 'measure' => 0, 'hidden' => 0, 'name' => 'Укроп 100г', 'quantity' => 11.000, 'price' => 25.99],
        ['id' => 1050400029, 'category' => 3, 'measure' => 0, 'hidden' => 0, 'name' => 'Укроп 50г', 'quantity' => 15.000, 'price' => 25.99],
        ['id' => 1050400028, 'category' => 3, 'measure' => 0, 'hidden' => 0, 'name' => 'Укроп свежий 250г', 'quantity' => 13.000, 'price' => 55.99],
        ['id' => 7040900007, 'category' => 8, 'measure' => 1, 'hidden' => 0, 'name' => 'Хребты Семги горячего копчения', 'quantity' => 9.360, 'price' => 659.99],
    ];

    public function __construct(
        private MeasureRepository $measureRepository,
        private CategoryRepository $categoryRepository,
    ) {
    }

    public function load(ObjectManager $manager): void
    {
        foreach (self::DATA as $item) {
            $product = new Product(
                $this->categoryRepository->find($item['category']),
                $this->measureRepository->find($item['measure']),
            );

            $product->setId($item['id']);
            $product
                ->setHidden($item['hidden'] === 1)
                ->setName($item['name'])
                ->setQuantity($item['quantity'])
                ->setPrice($item['price'])
            ;

            $manager->persist($product);
        }

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [
            MeasureFixtures::class,
            CategoryFixtures::class,
        ];
    }
}
