<?php

declare(strict_types=1);

namespace App\Infrastructure;

use App\Api\Request\RequestDtoInterface;
use App\Exception\CommonException;
use Exception;
use ReflectionClass;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class RequestDtoResolver implements ArgumentValueResolverInterface
{
    public function __construct(
        private ValidatorInterface $validator,
        private SerializerInterface $serializer,
    ) {
    }

    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        $type = $argument->getType();

        if (empty($type) || !class_exists($type)) {
            return false;
        }

        try {
            $reflection = new ReflectionClass($type);
        } catch (Exception) {
            return false;
        }

        return $reflection->implementsInterface(RequestDtoInterface::class);
    }

    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        if (($class = $argument->getType()) === null || !class_exists($class)) {
            throw CommonException::internal();
        }

        /** @var mixed $object */
        $object = $this->serializer->deserialize($request->getContent(), $class, 'json');

        if (is_object($object)) {
            $this->validateObject($object);
        }

        yield $object;
    }

    private function validateObject(object $object): void
    {
        $errors = $this->validator->validate($object);

        if (count($errors) > 0) {
            throw CommonException::validate();
        }
    }
}
