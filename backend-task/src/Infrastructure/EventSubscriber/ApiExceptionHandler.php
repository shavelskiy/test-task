<?php

declare(strict_types=1);

namespace App\Infrastructure\EventSubscriber;

use App\Exception\BaseException;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\KernelEvents;
use Throwable;

class ApiExceptionHandler implements EventSubscriberInterface
{
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::EXCEPTION => 'onKernelException',
        ];
    }

    public function onKernelException(ExceptionEvent $event): void
    {
        $exception = $event->getThrowable();

        $response = new JsonResponse([
            'message' => $exception->getMessage(),
            'code' => $exception->getCode(),
            'data' => $exception instanceof BaseException ? $exception->getData() : [],
            'trace' => $exception->getTrace(),
        ], $this->getStatusCode($exception));

        $event->setResponse($response);
    }

    private function getStatusCode(Throwable $exception): int
    {
        if ($exception instanceof HttpException) {
            return $exception->getStatusCode();
        }

        if ($exception instanceof BaseException) {
            return $exception->getStatusCode();
        }

        return 400;
    }
}
