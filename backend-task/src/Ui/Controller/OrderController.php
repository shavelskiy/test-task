<?php

declare(strict_types=1);

namespace App\Ui\Controller;

use App\Api\Response\OrderResponse;
use App\Application\Command\OrderQuery;
use App\Application\CommandBusInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Attributes as OA;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[OA\Response(
    response: 200,
    description: 'Список заказов',
    content: new OA\JsonContent(
        type: 'array',
        items: new OA\Items(ref: new Model(type: OrderResponse::class))
    )
)]

#[Route(path: '/order', name: self::class, methods: 'GET')]
class OrderController implements ControllerInterface
{
    public function __construct(
        private CommandBusInterface $commandBus,
    ) {
    }

    public function __invoke(): Response
    {
        $this->commandBus->handle($query = new OrderQuery());

        $result = [];
        foreach ($query->getResult() as $order) {
            $result[] = new OrderResponse($order);
        }

        return new JsonResponse($result);
    }
}
