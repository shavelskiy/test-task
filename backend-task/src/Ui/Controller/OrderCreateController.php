<?php

declare(strict_types=1);

namespace App\Ui\Controller;

use App\Api\Request\OrderCreateRequest;
use App\Api\Response\OrderResponse;
use App\Application\Command\OrderCreateCommand;
use App\Application\CommandBusInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Attributes as OA;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[OA\RequestBody(
    content: new OA\JsonContent(ref: new Model(type: OrderCreateRequest::class))
)]
#[OA\Response(
    response: 200,
    description: 'Заказ успешно создан',
    content: new OA\JsonContent(ref: new Model(type: OrderResponse::class))
)]

#[Route(path: '/order', name: self::class, methods: 'POST')]
class OrderCreateController implements ControllerInterface
{
    public function __construct(
        private CommandBusInterface $commandBus,
    ) {
    }

    public function __invoke(OrderCreateRequest $request): Response
    {
        $this->commandBus->handle($command = new OrderCreateCommand($request));

        return new JsonResponse(
            new OrderResponse($command->getResult()),
        );
    }
}
