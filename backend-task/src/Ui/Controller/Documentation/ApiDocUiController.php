<?php

declare(strict_types=1);

namespace App\Ui\Controller\Documentation;

use App\Ui\Controller\ControllerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/doc', name: self::class, methods: 'GET')]
class ApiDocUiController implements ControllerInterface
{
    public function __invoke(): Response
    {
        $html = <<<'EOF'
            <!DOCTYPE html>
            <html>
            <head>
                <title>Api Doc</title>
                <meta charset="utf-8"/>
                <meta name="viewport" content="width=device-width, initial-scale=1">
                <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,700|Roboto:300,400,700" rel="stylesheet">
            
                <style>
                    body {
                        margin: 0;
                        padding: 0;
                    }
                </style>
            </head>
            <body>
            
            <redoc spec-url='/v1/doc.json'></redoc>
            <script src="https://cdn.jsdelivr.net/npm/redoc@next/bundles/redoc.standalone.js"></script>
            </body>
            </html>
        EOF;

        return new Response($html);
    }
}
