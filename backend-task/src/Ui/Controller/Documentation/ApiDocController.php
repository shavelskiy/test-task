<?php

declare(strict_types=1);

namespace App\Ui\Controller\Documentation;

use App\Ui\Controller\ControllerInterface;
use Nelmio\ApiDocBundle\ApiDocGenerator;
use OpenApi\Annotations\Operation;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/doc.json', name: self::class, methods: 'GET')]
class ApiDocController implements ControllerInterface
{
    public function __construct(
        private ApiDocGenerator $generator,
    ) {
    }

    public function __invoke(): Response
    {
        $result = $this->generator->generate();

        foreach ($result->paths as $path) {
            $this->handleOperation($path->get);
            $this->handleOperation($path->post);
            $this->handleOperation($path->put);
            $this->handleOperation($path->patch);
            $this->handleOperation($path->delete);
        }

        return new JsonResponse($result);
    }

    private function handleOperation(mixed $operation): void
    {
        if (!$operation instanceof Operation) {
            return;
        }

        $operation->summary = $operation->path;
        $operation->operationId = substr(md5($operation->path . $operation->method), 0, 10);
    }
}
