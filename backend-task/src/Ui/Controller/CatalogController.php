<?php

declare(strict_types=1);

namespace App\Ui\Controller;

use App\Api\Factory\CatalogResponseFactory;
use App\Api\Response\CatalogResponse;
use App\Application\Command\CatalogQuery;
use App\Application\CommandBusInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Attributes as OA;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[OA\Response(
    response: 200,
    description: 'Данные каталога',
    content: new OA\JsonContent(ref: new Model(type: CatalogResponse::class))
)]
#[Route(path: '/catalog', name: self::class, methods: 'GET')]
class CatalogController implements ControllerInterface
{
    public function __construct(
        private CommandBusInterface $commandBus,
        private CatalogResponseFactory $catalogResponseFactory,
    ) {
    }

    public function __invoke(): Response
    {
        $this->commandBus->handle($query = new CatalogQuery());

        return new JsonResponse(
            $this->catalogResponseFactory->craete($query->getResult()),
        );
    }
}
