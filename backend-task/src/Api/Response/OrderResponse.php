<?php

declare(strict_types=1);

namespace App\Api\Response;

use App\Infrastructure\Entity\Order;
use OpenApi\Attributes as OA;

class OrderResponse
{
    #[OA\Property(description: 'Id', example: 2)]
    public int $id;

    #[OA\Property(description: 'Контактное лицо')]
    public OrderPersonResponse $person;

    #[OA\Property(description: 'Доставка')]
    public OrderDeliveryResponse $delivery;

    #[OA\Property(description: 'Корзина')]
    /** @var OrderBasketItemResponse[] */
    public array $basket = [];

    #[OA\Property(description: 'Дата создания', example: '2022-07-30T14:34:31+03:00')]
    public string $createdAt;

    public function __construct(Order $order)
    {
        $this->id = $order->getId();
        $this->person = new OrderPersonResponse($order->getPerson());
        $this->delivery = new OrderDeliveryResponse($order->getDelivery());

        foreach ($order->getBasket() as $item) {
            $this->basket[] = new OrderBasketItemResponse($item);
        }

        $this->createdAt = $order->getCreatedAt()->format('c');
    }
}
