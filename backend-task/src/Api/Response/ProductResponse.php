<?php

declare(strict_types=1);

namespace App\Api\Response;

use App\Infrastructure\Entity\Product;
use OpenApi\Attributes as OA;

class ProductResponse
{
    #[OA\Property(description: 'Id', example: 2)]
    public int $id;

    #[OA\Property(description: 'Мера измерения')]
    public MeasureResponse $measure;

    #[OA\Property(description: 'Название', example: 'Молоко')]
    public string $name;

    #[OA\Property(description: 'Количество', example: 1.33)]
    public float $quantity;

    #[OA\Property(description: 'Цена', example: 234.33)]
    public float $price;

    public function __construct(Product $product)
    {
        $this->id = $product->getId();
        $this->measure = new MeasureResponse($product->getMeasure());
        $this->name = $product->getName();
        $this->quantity = $product->getQuantity();
        $this->price = $product->getPrice();
    }
}
