<?php

declare(strict_types=1);

namespace App\Api\Response;

use App\Infrastructure\Entity\Category;
use App\Infrastructure\Entity\Product;
use OpenApi\Attributes as OA;

class CatalogItemResponse
{
    #[OA\Property(description: 'Категория')]
    public CategoryResponse $category;

    #[OA\Property(description: 'Товары')]
    /** @var ProductResponse[] */
    public array $products = [];

    /** @param Product[] $products */
    public function __construct(Category $category, array $products)
    {
        $this->category = new CategoryResponse($category);

        foreach ($products as $product) {
            $this->products[] = new ProductResponse($product);
        }
    }
}
