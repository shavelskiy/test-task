<?php

declare(strict_types=1);

namespace App\Api\Response;

use OpenApi\Attributes as OA;

class CatalogResponse
{
    #[OA\Property(description: 'Элементы каталога')]
    /** @var CatalogItemResponse[] */
    public array $items;

    /** @param CatalogItemResponse[] $items */
    public function __construct(array $items)
    {
        $this->items = $items;
    }
}
