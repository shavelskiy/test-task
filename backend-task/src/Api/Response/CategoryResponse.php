<?php

declare(strict_types=1);

namespace App\Api\Response;

use App\Infrastructure\Entity\Category;
use OpenApi\Attributes as OA;

class CategoryResponse
{
    #[OA\Property(description: 'Id', example: 2)]
    public int $id;

    #[OA\Property(description: 'Название', example: 'Продукты')]
    public string $name;

    public function __construct(Category $category)
    {
        $this->id = $category->getId();
        $this->name = $category->getName();
    }
}
