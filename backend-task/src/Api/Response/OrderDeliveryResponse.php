<?php

declare(strict_types=1);

namespace App\Api\Response;

use App\Infrastructure\Entity\OrderDelivery;
use OpenApi\Attributes as OA;

class OrderDeliveryResponse
{
    #[OA\Property(description: 'Город', example: 'Москва')]
    public string $city;

    #[OA\Property(description: 'Адрес', example: '+79998884443322')]
    public string $address;

    #[OA\Property(description: 'Комментарий', example: 'test@test.test')]
    public ?string $comment;

    public function __construct(OrderDelivery $delivery)
    {
        $this->city = $delivery->getCity();
        $this->address = $delivery->getAddress();
        $this->comment = $delivery->getComment();
    }
}
