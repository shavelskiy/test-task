<?php

declare(strict_types=1);

namespace App\Api\Response;

use App\Infrastructure\Entity\OrderPerson;
use OpenApi\Attributes as OA;

class OrderPersonResponse
{
    #[OA\Property(description: 'Имя', example: 'Иванов Иван')]
    public string $name;

    #[OA\Property(description: 'Телефон', example: '+79998884443322')]
    public string $phone;

    #[OA\Property(description: 'Почта', example: 'test@test.test')]
    public string $email;

    public function __construct(OrderPerson $person)
    {
        $this->name = $person->getName();
        $this->phone = $person->getPhone();
        $this->email = $person->getEmail();
    }
}
