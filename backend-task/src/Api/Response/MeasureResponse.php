<?php

declare(strict_types=1);

namespace App\Api\Response;

use App\Infrastructure\Entity\Measure;
use OpenApi\Attributes as OA;

class MeasureResponse
{
    #[OA\Property(description: 'Id', example: 2)]
    public int $id;

    #[OA\Property(description: 'Название', example: 'шт')]
    public string $name;

    public function __construct(Measure $measure)
    {
        $this->id = $measure->getId();
        $this->name = $measure->getName();
    }
}
