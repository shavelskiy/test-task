<?php

declare(strict_types=1);

namespace App\Api\Response;

use App\Infrastructure\Entity\OrderBasket;
use OpenApi\Attributes as OA;

class OrderBasketItemResponse
{
    #[OA\Property(description: 'Количество', example: 2)]
    public int $count;

    #[OA\Property(description: 'Товар')]
    public ProductResponse $product;

    #[OA\Property(description: 'Цена', example: 32.2)]
    public float $price;

    public function __construct(OrderBasket $basket)
    {
        $this->count = $basket->getCount();
        $this->product = new ProductResponse($basket->getProduct());
        $this->price = $basket->getPrice();
    }
}
