<?php

declare(strict_types=1);

namespace App\Api\Factory;

use App\Api\Response\CatalogItemResponse;
use App\Api\Response\CatalogResponse;
use App\Infrastructure\Entity\Product;

class CatalogResponseFactory
{
    /** @param Product[] $products */
    public function craete(array $products): CatalogResponse
    {
        $aggregation = [];

        foreach ($products as $product) {
            $category = $product->getCategory();
            if (!isset($aggregation[$category->getId()])) {
                $aggregation[$category->getId()] = [
                    'category' => $category,
                    'products' => [],
                ];
            }

            $aggregation[$category->getId()]['products'][] = $product;
        }

        $result = [];
        foreach ($aggregation as $item) {
            $result[] = new CatalogItemResponse($item['category'], $item['products']);
        }

        return new CatalogResponse($result);
    }
}
