<?php

declare(strict_types=1);

namespace App\Api\Request;

use OpenApi\Attributes as OA;

class OrderDeliveryRequest
{
    public function __construct(
        #[OA\Property(description: 'Город доставки', example: 'Москва')]
        private string $city,
        #[OA\Property(description: 'Адрес доставки', example: 'большая татарская 25, 43')]
        private string $address,
        #[OA\Property(description: 'Комментарий к доставке', example: 'Во дворе есть шлагбаум')]
        private ?string $comment = null,
    ) {
    }

    public function getCity(): string
    {
        return $this->city;
    }

    public function getAddress(): string
    {
        return $this->address;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }
}
