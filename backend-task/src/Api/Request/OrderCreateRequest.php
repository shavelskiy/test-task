<?php

declare(strict_types=1);

namespace App\Api\Request;

use OpenApi\Attributes as OA;

class OrderCreateRequest implements RequestDtoInterface
{
    /**
     * @param OrderBasketItemRequest[] $basket
     */
    public function __construct(
        #[OA\Property(description: 'Контактное лицо')]
        private OrderPersonRequest $person,
        #[OA\Property(description: 'Доставка заказа')]
        private OrderDeliveryRequest $delivery,
        #[OA\Property(description: 'Корзина заказа')]
        private array $basket,
    ) {
    }

    public function getPerson(): OrderPersonRequest
    {
        return $this->person;
    }

    public function getDelivery(): OrderDeliveryRequest
    {
        return $this->delivery;
    }

    /** @return OrderBasketItemRequest[] */
    public function getBasket(): array
    {
        return $this->basket;
    }
}
