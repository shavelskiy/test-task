<?php

declare(strict_types=1);

namespace App\Api\Request;

use OpenApi\Attributes as OA;

class OrderBasketItemRequest
{
    public function __construct(
        #[OA\Property(description: 'Id товара', example: 34)]
        private int $productId,
        #[OA\Property(description: 'Количество товара в корзине', example: 3)]
        private int $count,
    ) {
    }

    public function getProductId(): int
    {
        return $this->productId;
    }

    public function getCount(): int
    {
        return $this->count;
    }
}
