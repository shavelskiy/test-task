<?php

declare(strict_types=1);

namespace App\Api\Request;

use OpenApi\Attributes as OA;

class OrderPersonRequest
{
    public function __construct(
        #[OA\Property(description: 'Имя', example: 'Иванов Иван')]
        private string $name,
        #[OA\Property(description: 'Телефон', example: '+79993332211')]
        private string $phone,
        #[OA\Property(description: 'Почта', example: 'test@test.test')]
        private string $email,
    ) {
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }

    public function getEmail(): string
    {
        return $this->email;
    }
}
