<?php

declare(strict_types=1);

namespace App\Exception;

class CommonException extends BaseException
{
    public static function validate(): self
    {
        return new self('validate exception', 100);
    }

    public static function internal(string $message = ''): self
    {
        return new self(sprintf('internal exception %s', $message), 101, statusCode: 500);
    }
}
