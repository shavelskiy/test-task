<?php

declare(strict_types=1);

namespace App\Exception;

class RepositoryException extends BaseException
{
    public static function notFound(): self
    {
        return new self('entity not found', 300);
    }

    public static function notFullEntity(string $field): self
    {
        return new self(sprintf('entity not has field: %s', $field), 301);
    }
}
