<?php

declare(strict_types=1);

namespace App\Exception;

class CommandException extends BaseException
{
    public static function commandHandler(): self
    {
        return new self('handler_not_support', 200);
    }

    public static function result(): self
    {
        return new self('command not has result', 201);
    }

    public static function handlerNotFound(string $command): self
    {
        return new self(sprintf('could not find handler for command %s', $command), 202);
    }
}
