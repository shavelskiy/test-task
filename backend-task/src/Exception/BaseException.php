<?php

declare(strict_types=1);

namespace App\Exception;

use RuntimeException;

abstract class BaseException extends RuntimeException
{
    private array $data;
    private int $statusCode;

    public function __construct(string $message, int $code = 0, array $data = [], int $statusCode = 400)
    {
        parent::__construct($message, $code);
        $this->data = $data;
        $this->statusCode = $statusCode;
    }

    public function getData(): array
    {
        return $this->data;
    }

    public function getStatusCode(): int
    {
        return $this->statusCode;
    }
}
